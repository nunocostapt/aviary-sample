package com.aviary.launcher3d;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.mazmellow.aviary.R;

public class ShareImageActivity extends Activity{
	
	Button shareBtn,saveBtn;
	ImageView successImage;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.share);
		
		saveBtn = (Button)findViewById(R.id.buttonSave);
		saveBtn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				captureResultImage();
			}
		});
		
		shareBtn = (Button)findViewById(R.id.buttonShare);
		shareBtn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Bitmap icon = MainActivity.bitmap;
				Intent share = new Intent(Intent.ACTION_SEND);
				share.setType("image/jpeg");
				ByteArrayOutputStream bytes = new ByteArrayOutputStream();
				icon.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
				
				File f = new File(Environment.getExternalStorageDirectory() + File.separator + "temporary_file.jpg");
				if(f.exists()){
					f.delete();
				}
				try {
				    f.createNewFile();
				    FileOutputStream fo = new FileOutputStream(f);
				    fo.write(bytes.toByteArray());
				} catch (IOException e) {                       
				        e.printStackTrace();
				}
				share.putExtra(Intent.EXTRA_STREAM, Uri.parse("file:///sdcard/temporary_file.jpg"));
				startActivity(Intent.createChooser(share, "Share Image"));
			}
		});
		
		successImage = (ImageView)findViewById(R.id.imageSuccess);
		if(MainActivity.bitmap!=null){
			successImage.setImageBitmap(MainActivity.bitmap);
		}
		
		
	}
	
	void captureResultImage() {
		// image naming and path to include sd card appending name you choose
		// for file
		File folder = new File(Environment.getExternalStorageDirectory() + "/ThaiSticker");
		if (!folder.exists()) {
		    if(folder.mkdir()){
		    	saveImage();
		    }
		}else{
			saveImage();
		}
	}
	
	void saveImage(){
		String mPath = Environment.getExternalStorageDirectory().toString() + "/ThaiSticker"
				+ "/IMG_" + System.currentTimeMillis()+".jpg";

		// create bitmap screen capture
		Bitmap bitmap;
		View v1 = successImage;//bgContainer.getRootView();
		successImage.setDrawingCacheEnabled(true);
		bitmap = Bitmap.createBitmap(v1.getDrawingCache());
		
		System.out.println("width = "+bitmap.getWidth());
		System.out.println("height = "+bitmap.getHeight());
		
		v1.setDrawingCacheEnabled(false);

		OutputStream fout = null;
		File imageFile = new File(mPath);

		try {
			fout = new FileOutputStream(imageFile);
			bitmap.compress(Bitmap.CompressFormat.JPEG, 90, fout);
			fout.flush();
			fout.close();
			
			Toast.makeText(this,"Save Image Success", Toast.LENGTH_SHORT).show();

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
