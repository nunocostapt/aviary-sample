package com.aviary.launcher3d;

import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.RelativeLayout;

public class DragTouchListenner implements View.OnTouchListener{
	
	ImageView ima1;
	RelativeLayout.LayoutParams layoutParams1;
	int windowwidth,windowheight;
	int w1,h1;
	boolean isGetVaule;
	
	public DragTouchListenner(int _WW, int _WH, ImageView imageView) {
		// TODO Auto-generated constructor stub
		ima1 = imageView;
		windowwidth = _WW;
		windowheight = _WH;
		
		ViewTreeObserver vto = ima1.getViewTreeObserver();
		vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
			public boolean onPreDraw() {
				if (!isGetVaule) {
					h1 = ima1.getMeasuredHeight();
					w1 = ima1.getMeasuredWidth();
					isGetVaule = true;
				}
				return true;
			}
		});
	}
	
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		layoutParams1 = (RelativeLayout.LayoutParams) ima1
				.getLayoutParams();
		switch (event.getActionMasked()) {
		case MotionEvent.ACTION_DOWN:
			break;
		case MotionEvent.ACTION_MOVE:
			int x_cord = (int) event.getRawX();
			int y_cord = (int) event.getRawY();

			if (x_cord > windowwidth - w1 / 2) {
				x_cord = windowwidth - w1 / 2;
			}
			if (y_cord >= windowheight - h1 / 2) {
				y_cord = windowheight - h1 / 2;
			}

			layoutParams1.leftMargin = x_cord - w1 / 2;
			if (layoutParams1.leftMargin < 0) {
				layoutParams1.leftMargin = 0;
			}

			layoutParams1.topMargin = y_cord - h1 / 2;
			if (layoutParams1.topMargin < 0) {
				layoutParams1.topMargin = 0;
			}
			if (layoutParams1.topMargin > windowheight - h1) {
				layoutParams1.topMargin = windowheight - h1;
			}

			// System.out.println("maz layoutParams1.leftMargin = "+layoutParams1.leftMargin+", layoutParams1.topMargin = "+layoutParams1.topMargin);
			// System.out.println("maz ------------");

			ima1.setLayoutParams(layoutParams1);
			break;
		default:
			break;
		}
		return true;
	}

}
